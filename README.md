# Detection-of-Chest-Diseases-from-X-Ray-Images-using-CNN

This study proposes a convolutional neural network model trained from scratch to classify and detect the presence of chest diseases mainly pneumonia, covid-19 and tuberculosis from a collection of chest X-ray image samples. 

# Dataset  
We have segregated and used the datasets from Omar Ali for [Tuberculosis](https://www.kaggle.com/sindalflekke/tb3000), Tawsifur Rahman for [COVID-19](https://www.kaggle.com/tawsifurrahman/covid19-radiography-database) and [Mendeley Data for Pneumonia and Normal](https://data.mendeley.com/datasets/rscbjbr9sj/2) Chest X-Ray Images.

Categories: Covid, Pneumonia, Tuberculosis and Normal.  
Total Size: 1.57 GB  
Total Images: 12426  
Folders: The dataset is divided into test and train folders.  

<details><summary>test:</summary>  
The number of images in their respective folders are:  

COVID: 409  
PNEUMONIA: 390  
TUBERCULOSIS: 400  
NORMAL: 234 
</details>
 
<details><summary>train:</summary>  
The number of images in their respective folders are:  

COVID: 3161  
PNEUMONIA: 3883  
TUBERCULOSIS: 2600  
NORMAL: 1349  

